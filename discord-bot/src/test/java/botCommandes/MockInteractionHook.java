package botCommandes;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.interactions.Interaction;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.components.LayoutComponent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.WebhookMessageCreateAction;
import net.dv8tion.jda.api.requests.restaction.WebhookMessageEditAction;
import net.dv8tion.jda.api.utils.AttachedFile;
import net.dv8tion.jda.api.utils.FileUpload;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import net.dv8tion.jda.api.utils.messages.MessageEditData;

import java.util.Collection;

/**
 * Classe permettant de simuler un InteractionHook
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public class MockInteractionHook implements InteractionHook {

    /**
     * Le message du hook
     */
    private String message;

    /**
     * Initialise le MockInteractionHook
     */
    public MockInteractionHook() {
        this.message = "";
    }

    /**
     * Permet d'obtenir le message du Hook
     *
     * @return Le message obtenu par le Hook
     */
    public String getMessage() {
        return message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Interaction getInteraction() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InteractionHook setEphemeral(boolean ephemeral) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JDA getJDA() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RestAction<Message> retrieveOriginal() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageCreateAction<Message> sendMessage(String content) {
        message = content;
        return new MockWebHookMessageCreateAction();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageCreateAction<Message> sendMessage(MessageCreateData message) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageCreateAction<Message> sendMessageEmbeds(Collection<? extends MessageEmbed> embeds) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageCreateAction<Message> sendMessageComponents(Collection<? extends LayoutComponent> components) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageCreateAction<Message> sendFiles(Collection<? extends FileUpload> files) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageEditAction<Message> editMessageById(String messageId, String content) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageEditAction<Message> editMessageById(String messageId, MessageEditData message) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageEditAction<Message> editMessageEmbedsById(String messageId, Collection<? extends MessageEmbed> embeds) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageEditAction<Message> editMessageComponentsById(String messageId, Collection<? extends LayoutComponent> components) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebhookMessageEditAction<Message> editMessageAttachmentsById(String messageId, Collection<? extends AttachedFile> attachments) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RestAction<Void> deleteMessageById(String messageId) {
        return null;
    }
}
