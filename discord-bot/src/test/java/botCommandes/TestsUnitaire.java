package botCommandes;

import discordBot.BotCommandes;
import enums.Commande;
import enums.OptionCommande;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.requests.restaction.interactions.ReplyCallbackAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import serpAPI.IGestionSerpAPI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Tests unitaire pour le bot Discord
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public class TestsUnitaire {

    /**
     * Mock de l'interface IGestionSerpAPi
     */
    @Mock
    IGestionSerpAPI gestionSerpAPIMock;

    /**
     * Mock de la classe SlashCommandInteractionEvent
     */
    @Mock
    SlashCommandInteractionEvent slashEvent;

    /**
     * Mock de la classe OptionMapping pour l'option « mot »
     */
    @Mock
    OptionMapping motOption;

    /**
     * Mock de la classe OptionMapping pour l'option « aTraduire »
     */
    @Mock
    OptionMapping aTraduireOption;

    /**
     * Mock de la classe OptionMapping pour l'option « langueutilisee »
     */
    @Mock
    OptionMapping langueUtiliseeOption;

    /**
     * Mock de la classe OptionMapping pour l'option « langueTraduit »
     */
    @Mock
    OptionMapping langueTraduitOption;

    /**
     * Mock de la classe OptionMapping pour l'option « nombre »
     */
    @Mock
    OptionMapping nombreOption;

    /**
     * Mock de la classe OptionMapping pour l'option « uniteMesure »
     */
    @Mock
    OptionMapping uniteMesureOption;

    /**
     * Mock de la classe OptionMapping pour l'option « uniteMesureVoulue »
     */
    @Mock
    OptionMapping uniteMesureVoulueOption;

    /**
     * Initialise les Mocks avant l'exécution de chaque test
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    /**
     * Test normal pour la commande /definition
     */
    @Test
    public void testSlashCommandEventDefinitionNormal() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.DEFINITION.transformerEnMinuscule());

        when(motOption.getType()).thenReturn(OptionType.STRING);
        when(motOption.getAsString()).thenReturn("facile");

        when(slashEvent.getOption(OptionCommande.MOT.transformerEnMinuscule())).thenReturn(motOption);
        
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });

        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });

        botCommandes.onSlashCommandInteraction(slashEvent);

        String messageAttendu = "\"Définition facile\"";

        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test erroné pour la commande /definition
     */
    @Test
    public void testSlashCommandEventDefinitionErrone() {
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.DEFINITION.transformerEnMinuscule());

        when(motOption.getType()).thenReturn(OptionType.STRING);
        when(motOption.getAsString()).thenReturn("chat");

        when(slashEvent.getOption(OptionCommande.MOT.transformerEnMinuscule())).thenReturn(motOption);

        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });

        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });

        botCommandes.onSlashCommandInteraction(slashEvent);

        String messageAttendu = "Définition introuvable";

        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /definition avec le mot vide
     */
    @Test
    public void testSlashCommandEventDefinitionLimite() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.DEFINITION.transformerEnMinuscule());

        when(motOption.getType()).thenReturn(OptionType.STRING);
        when(motOption.getAsString()).thenReturn("");

        when(slashEvent.getOption(OptionCommande.MOT.transformerEnMinuscule())).thenReturn(motOption);

        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });

        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });

        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });

        botCommandes.onSlashCommandInteraction(slashEvent);

        String messageAttendu = "";

        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test normal pour la commande /traduction
     */
    @Test
    public void testSlashCommandEventTraductionNormal() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);

        when(slashEvent.getName()).thenReturn(Commande.TRADUIRE.transformerEnMinuscule());

        when(aTraduireOption.getType()).thenReturn(OptionType.STRING);
        when(aTraduireOption.getAsString()).thenReturn("chat");
        when(slashEvent.getOption(OptionCommande.ATRADUIRE.transformerEnMinuscule())).thenReturn(aTraduireOption);

        when(langueUtiliseeOption.getType()).thenReturn(OptionType.STRING);
        when(langueUtiliseeOption.getAsString()).thenReturn("French");
        when(slashEvent.getOption(OptionCommande.LANGUEUTILISEE.transformerEnMinuscule()))
                .thenReturn(langueUtiliseeOption);

        when(langueTraduitOption.getType()).thenReturn(OptionType.STRING);
        when(langueTraduitOption.getAsString()).thenReturn("English");
        when(slashEvent.getOption(OptionCommande.LANGUETRADUIT.transformerEnMinuscule()))
                .thenReturn(langueTraduitOption);

        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "Translate chat from French to English";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test erroné pour la commande /traduction
     */
    @Test
    public void testSlashCommandEventTraductionErrone() {
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);

        when(slashEvent.getName()).thenReturn(Commande.TRADUIRE.transformerEnMinuscule());

        when(aTraduireOption.getType()).thenReturn(OptionType.STRING);
        when(aTraduireOption.getAsString()).thenReturn("chat");
        when(slashEvent.getOption(OptionCommande.ATRADUIRE.transformerEnMinuscule())).thenReturn(aTraduireOption);

        when(langueUtiliseeOption.getType()).thenReturn(OptionType.STRING);
        when(langueUtiliseeOption.getAsString()).thenReturn("french");
        when(slashEvent.getOption(OptionCommande.LANGUEUTILISEE.transformerEnMinuscule()))
                .thenReturn(langueUtiliseeOption);

        when(langueTraduitOption.getType()).thenReturn(OptionType.STRING);
        when(langueTraduitOption.getAsString()).thenReturn("english");
        when(slashEvent.getOption(OptionCommande.LANGUETRADUIT.transformerEnMinuscule()))
                .thenReturn(langueTraduitOption);

        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });

        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });

        botCommandes.onSlashCommandInteraction(slashEvent);

        String messageAttendu = "Traduction impossible";

        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /traduction avec l'option aTraduire vide
     */
    @Test
    public void testSlashCommandEventTraductionLimiteATraduireVide() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.TRADUIRE.transformerEnMinuscule());

        when(aTraduireOption.getType()).thenReturn(OptionType.STRING);
        when(aTraduireOption.getAsString()).thenReturn("");
        when(slashEvent.getOption(OptionCommande.ATRADUIRE.transformerEnMinuscule())).thenReturn(aTraduireOption);

        when(langueUtiliseeOption.getType()).thenReturn(OptionType.STRING);
        when(langueUtiliseeOption.getAsString()).thenReturn("french");
        when(slashEvent.getOption(OptionCommande.LANGUEUTILISEE.transformerEnMinuscule()))
                .thenReturn(langueUtiliseeOption);

        when(langueTraduitOption.getType()).thenReturn(OptionType.STRING);
        when(langueTraduitOption.getAsString()).thenReturn("english");
        when(slashEvent.getOption(OptionCommande.LANGUETRADUIT.transformerEnMinuscule()))
                .thenReturn(langueTraduitOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /traduction avec l'option langueutilisee vide
     */
    @Test
    public void testSlashCommandEventTraductionLimiteLangueUtiliseeVide() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.TRADUIRE.transformerEnMinuscule());
        when(aTraduireOption.getType()).thenReturn(OptionType.STRING);
        when(aTraduireOption.getAsString()).thenReturn("chat");
        when(slashEvent.getOption(OptionCommande.ATRADUIRE.transformerEnMinuscule())).thenReturn(aTraduireOption);

        when(langueUtiliseeOption.getType()).thenReturn(OptionType.STRING);
        when(langueUtiliseeOption.getAsString()).thenReturn("");
        when(slashEvent.getOption(OptionCommande.LANGUEUTILISEE.transformerEnMinuscule()))
                .thenReturn(langueUtiliseeOption);

        when(langueTraduitOption.getType()).thenReturn(OptionType.STRING);
        when(langueTraduitOption.getAsString()).thenReturn("english");
        when(slashEvent.getOption(OptionCommande.LANGUETRADUIT.transformerEnMinuscule()))
                .thenReturn(langueTraduitOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /traduction avec l'option LangueTraduit vide
     */
    @Test
    public void testSlashCommandEventTraductionLimiteLangueTraduitVide() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.TRADUIRE.transformerEnMinuscule());
        when(aTraduireOption.getType()).thenReturn(OptionType.STRING);
        when(aTraduireOption.getAsString()).thenReturn("chat");
        when(slashEvent.getOption(OptionCommande.ATRADUIRE.transformerEnMinuscule())).thenReturn(aTraduireOption);

        when(langueUtiliseeOption.getType()).thenReturn(OptionType.STRING);
        when(langueUtiliseeOption.getAsString()).thenReturn("french");
        when(slashEvent.getOption(OptionCommande.LANGUEUTILISEE.transformerEnMinuscule()))
                .thenReturn(langueUtiliseeOption);

        when(langueTraduitOption.getType()).thenReturn(OptionType.STRING);
        when(langueTraduitOption.getAsString()).thenReturn("");
        when(slashEvent.getOption(OptionCommande.LANGUETRADUIT.transformerEnMinuscule()))
                .thenReturn(langueTraduitOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /traduction avec l'option langueutilisee identique à l'option languetraduit
     */
    @Test
    public void testSlashCommandEventTraductionLimiteLangueUtiliseeEtLangueTraduitIdentique() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.TRADUIRE.transformerEnMinuscule());
        when(aTraduireOption.getType()).thenReturn(OptionType.STRING);
        when(aTraduireOption.getAsString()).thenReturn("chat");
        when(slashEvent.getOption(OptionCommande.ATRADUIRE.transformerEnMinuscule())).thenReturn(aTraduireOption);

        when(langueUtiliseeOption.getType()).thenReturn(OptionType.STRING);
        when(langueUtiliseeOption.getAsString()).thenReturn("french");
        when(slashEvent.getOption(OptionCommande.LANGUEUTILISEE.transformerEnMinuscule()))
                .thenReturn(langueUtiliseeOption);

        when(langueTraduitOption.getType()).thenReturn(OptionType.STRING);
        when(langueTraduitOption.getAsString()).thenReturn("french");
        when(slashEvent.getOption(OptionCommande.LANGUETRADUIT.transformerEnMinuscule()))
                .thenReturn(langueTraduitOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test normal pour la commande /convertion
     */
    @Test
    public void testSlashCommandEventConvertionNormal() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.CONVERTIR.transformerEnMinuscule());

        when(nombreOption.getType()).thenReturn(OptionType.INTEGER);
        when(nombreOption.getAsInt()).thenReturn(10);
        when(slashEvent.getOption(OptionCommande.NOMBRE.transformerEnMinuscule())).thenReturn(nombreOption);

        when(uniteMesureOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureOption.getAsString()).thenReturn("kilomètre");
        when(slashEvent.getOption(OptionCommande.UNITEMESURE.transformerEnMinuscule()))
                .thenReturn(uniteMesureOption);

        when(uniteMesureVoulueOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureVoulueOption.getAsString()).thenReturn("mètre");
        when(slashEvent.getOption(OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule()))
                .thenReturn(uniteMesureVoulueOption);

        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "10 kilomètre to mètre";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test erronée pour la commande /convertion
     */
    @Test
    public void testSlashCommandEventConvertionErrone() {
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);

        when(slashEvent.getName()).thenReturn(Commande.CONVERTIR.transformerEnMinuscule());

        when(nombreOption.getType()).thenReturn(OptionType.INTEGER);
        when(nombreOption.getAsInt()).thenReturn(10);
        when(slashEvent.getOption(OptionCommande.NOMBRE.transformerEnMinuscule())).thenReturn(nombreOption);

        when(uniteMesureOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureOption.getAsString()).thenReturn("kilomètre");
        when(slashEvent.getOption(OptionCommande.UNITEMESURE.transformerEnMinuscule()))
                .thenReturn(uniteMesureOption);

        when(uniteMesureVoulueOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureVoulueOption.getAsString()).thenReturn("mètre");
        when(slashEvent.getOption(OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule()))
                .thenReturn(uniteMesureVoulueOption);

        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });

        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });

        botCommandes.onSlashCommandInteraction(slashEvent);

        String messageAttendu = "Convertion impossible";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /convertion avec un nombre 0
     */
    @Test
    public void testSlashCommandEventConvertionLimiteZero() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.CONVERTIR.transformerEnMinuscule());
        when(nombreOption.getType()).thenReturn(OptionType.INTEGER);
        when(nombreOption.getAsInt()).thenReturn(0);
        when(slashEvent.getOption(OptionCommande.NOMBRE.transformerEnMinuscule())).thenReturn(nombreOption);
        when(uniteMesureOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureOption.getAsString()).thenReturn("seconde");
        when(slashEvent.getOption(OptionCommande.UNITEMESURE.transformerEnMinuscule()))
                .thenReturn(uniteMesureOption);
        when(uniteMesureVoulueOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureVoulueOption.getAsString()).thenReturn("minute");
        when(slashEvent.getOption(OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule()))
                .thenReturn(uniteMesureVoulueOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }});
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /convertion avec l'unité de mesure vide
     */
    @Test
    public void testSlashCommandEventConvertionLimiteUniteMesureVide() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.CONVERTIR.transformerEnMinuscule());
        when(nombreOption.getType()).thenReturn(OptionType.INTEGER);
        when(nombreOption.getAsInt()).thenReturn(1);
        when(slashEvent.getOption(OptionCommande.NOMBRE.transformerEnMinuscule())).thenReturn(nombreOption);
        when(uniteMesureOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureOption.getAsString()).thenReturn("");
        when(slashEvent.getOption(OptionCommande.UNITEMESURE.transformerEnMinuscule()))
                .thenReturn(uniteMesureOption);
        when(uniteMesureVoulueOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureVoulueOption.getAsString()).thenReturn("minute");
        when(slashEvent.getOption(OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule()))
                .thenReturn(uniteMesureVoulueOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }});
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /convertion avec l'unité de mesure voulue vide
     */
    @Test
    public void testSlashCommandEventConvertionLimiteUniteMesureVoulueVide() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.CONVERTIR.transformerEnMinuscule());
        when(nombreOption.getType()).thenReturn(OptionType.INTEGER);
        when(nombreOption.getAsInt()).thenReturn(1);
        when(slashEvent.getOption(OptionCommande.NOMBRE.transformerEnMinuscule())).thenReturn(nombreOption);
        when(uniteMesureOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureOption.getAsString()).thenReturn("seconde");
        when(slashEvent.getOption(OptionCommande.UNITEMESURE.transformerEnMinuscule()))
                .thenReturn(uniteMesureOption);
        when(uniteMesureVoulueOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureVoulueOption.getAsString()).thenReturn("");
        when(slashEvent.getOption(OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule()))
                .thenReturn(uniteMesureVoulueOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }});
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }

    /**
     * Test limite pour la commande /convertion avec l'unité de mesure identique à l'unité de mesure voulue
     */
    @Test
    public void testSlashCommandEventConvertionLimiteUniteMesureEtUniteMesureVoulueIdentique() {
        when(gestionSerpAPIMock.ObtenirResultatSerpAPI(anyString(), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0);
            }
        });
        BotCommandes botCommandes = new BotCommandes(gestionSerpAPIMock);
        when(slashEvent.getName()).thenReturn(Commande.CONVERTIR.transformerEnMinuscule());

        when(nombreOption.getType()).thenReturn(OptionType.INTEGER);
        when(nombreOption.getAsInt()).thenReturn(1);
        when(slashEvent.getOption(OptionCommande.NOMBRE.transformerEnMinuscule())).thenReturn(nombreOption);

        when(uniteMesureOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureOption.getAsString()).thenReturn("seconde");
        when(slashEvent.getOption(OptionCommande.UNITEMESURE.transformerEnMinuscule()))
                .thenReturn(uniteMesureOption);

        when(uniteMesureVoulueOption.getType()).thenReturn(OptionType.STRING);
        when(uniteMesureVoulueOption.getAsString()).thenReturn("seconde");
        when(slashEvent.getOption(OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule()))
                .thenReturn(uniteMesureVoulueOption);
        when(slashEvent.deferReply()).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }
        });
        MockInteractionHook mockInteractionHook = new MockInteractionHook();
        when(slashEvent.getHook()).thenAnswer(new Answer<InteractionHook>() {
            @Override
            public InteractionHook answer(InvocationOnMock invocation) throws Throwable {
                return mockInteractionHook;
            }
        });
        when(slashEvent.reply(anyString())).thenAnswer(new Answer<ReplyCallbackAction>() {
            @Override
            public ReplyCallbackAction answer(InvocationOnMock invocation) throws Throwable {
                return new MockReplyCallbackAction();
            }});
        botCommandes.onSlashCommandInteraction(slashEvent);
        String messageAttendu = "";
        assertEquals(messageAttendu, mockInteractionHook.getMessage());
    }
}
