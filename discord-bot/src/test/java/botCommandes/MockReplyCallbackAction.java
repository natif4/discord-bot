package botCommandes;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.IMentionable;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.components.LayoutComponent;
import net.dv8tion.jda.api.requests.restaction.interactions.ReplyCallbackAction;
import net.dv8tion.jda.api.utils.FileUpload;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

/**
 * Classe permettant de simuler un ReplyCallbackAction
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public class MockReplyCallbackAction implements ReplyCallbackAction {

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction closeResources() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setEphemeral(boolean ephemeral) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setCheck(BooleanSupplier checks) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JDA getJDA() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void queue(Consumer<? super InteractionHook> success, Consumer<? super Throwable> failure) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InteractionHook complete(boolean shouldQueue) throws RateLimitedException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompletableFuture<InteractionHook> submit(boolean shouldQueue) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction addContent(String content) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction addEmbeds(Collection<? extends MessageEmbed> embeds) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction addComponents(Collection<? extends LayoutComponent> components) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction addFiles(Collection<? extends FileUpload> files) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FileUpload> getAttachments() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setTTS(boolean tts) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setContent(String content) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setEmbeds(Collection<? extends MessageEmbed> embeds) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setComponents(Collection<? extends LayoutComponent> components) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setSuppressEmbeds(boolean suppress) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setFiles(Collection<? extends FileUpload> files) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction mentionRepliedUser(boolean mention) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction setAllowedMentions(Collection<Message.MentionType> allowedMentions) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction mention(Collection<? extends IMentionable> mentions) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction mentionUsers(Collection<String> userIds) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReplyCallbackAction mentionRoles(Collection<String> roleIds) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContent() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MessageEmbed> getEmbeds() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LayoutComponent> getComponents() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSuppressEmbeds() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getMentionedUsers() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getMentionedRoles() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnumSet<Message.MentionType> getAllowedMentions() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMentionRepliedUser() {
        return false;
    }
}
