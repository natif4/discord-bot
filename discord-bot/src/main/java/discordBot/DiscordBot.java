package discordBot;

import configs.IDiscordConfiguration;
import enums.Commande;
import enums.OptionCommande;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.exceptions.InvalidTokenException;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.requests.restaction.CommandCreateAction;
import serpAPI.IGestionSerpAPI;

/**
 * Classe permettant de faire le démarrage du Discord Bot
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public class DiscordBot {

    /**
     * Le Token du DiscordBot
     */
    private final String token;

    /**
     * Le GuildId du DiscordBot
     */
    private final String guildId;

    /**
     * Permet de faire le pont entre la classe DiscordBot et la classe SerpAPI
     */
    private final IGestionSerpAPI gestionSerpAPI;

    /**
     * Initialise le DiscordBot
     *
     * @param discordConfiguration La configuration du serveur Discord
     * @param gestionSerpAPI Permet de faire le pont entre la classe DiscordBot et la classe SerpAPI
     */
    public DiscordBot(IDiscordConfiguration discordConfiguration, IGestionSerpAPI gestionSerpAPI) {
        token = discordConfiguration.getDiscordToken();
        guildId = discordConfiguration.getDiscordGuildId();
        this.gestionSerpAPI = gestionSerpAPI;
    }

    /**
     * Permet de démarrer le DiscordBot
     *
     * @throws InterruptedException Lorsqu'un thread est interrompu
     */
    public void Demarrer() throws InterruptedException {
        JDABuilder builder;
        JDA jda;

        try {
            builder = JDABuilder.createDefault(token);
            builder.addEventListeners(new BotCommandes(gestionSerpAPI));
            jda = builder.build().awaitReady();
        } catch (IllegalArgumentException illegalArgumentException) {
            System.err.println("Veuillez mettre le token à la première ligne du fichier token.txt");
            System.err.println();
            throw illegalArgumentException;
        } catch (InvalidTokenException invalidTokenException) {
            System.err.println("Le token spécifié dans le fichier token.txt est invalide");
            System.err.println();
            throw invalidTokenException;
        }

        try {
            Guild guild = jda.getGuildById(guildId);

            if (guild != null) {
                CreerCommandeDefinition(guild);
                CreerCommandeTraduire(guild);
                CreerCommandeConvertir(guild);
            }
        } catch (NumberFormatException numberFormatException) {
            System.err.println("Le GuildId spécifié dans le fichier guildId.txt est invalide");
            System.err.println();
            throw numberFormatException;
        } catch (IllegalArgumentException illegalArgumentException) {
            System.err.println("Veuillez mettre le guildId à la première ligne du fichier guildId.txt");
            System.err.println();
            throw  illegalArgumentException;
        }
    }

    /**
     * Créer la commande /definition
     *
     * La commande /definition permet d'obtenir une définition pour un mot
     *
     * @param guild Le guild du DiscordBot
     */
    private void CreerCommandeDefinition(Guild guild) {
        CommandCreateAction commandCreateAction = guild.upsertCommand(Commande.DEFINITION.transformerEnMinuscule(),
                "Obtenir la définition d'un mot");
        OptionData optionMot = new OptionData(OptionType.STRING, OptionCommande.MOT.transformerEnMinuscule(),
                "Le mot à définir", true);
        commandCreateAction = commandCreateAction.addOptions(optionMot);
        commandCreateAction.queue();
    }

    /**
     * Créer la commande /traduire
     *
     * La commande /traduire permet de traduire un mot ou une phrase courte en français vers le mot
     * ou la phrase correspondante en anglais (ou vice-versa)
     *
     * @param guild Le guild du DiscordBot
     */
    private void CreerCommandeTraduire(Guild guild) {
        CommandCreateAction commandCreateAction = guild.upsertCommand(Commande.TRADUIRE.transformerEnMinuscule(),
                "Traduit un mot ou une phrase en français ou en anglais");
        OptionData optionATraduire = new OptionData(OptionType.STRING,
                OptionCommande.ATRADUIRE.transformerEnMinuscule(), "Le mot ou la phrase à traduire",
                true);
        OptionData optionLangueUtilisee = new OptionData(OptionType.STRING,
                OptionCommande.LANGUEUTILISEE.transformerEnMinuscule(),
                "La langue utilisée pour le mot ou la phrase", true);
        optionLangueUtilisee.addChoice("Français", "french");
        optionLangueUtilisee.addChoice("Anglais", "english");
        OptionData optionLangueTraduit = new OptionData(OptionType.STRING,
                OptionCommande.LANGUETRADUIT.transformerEnMinuscule(),
                "La langue auquel le mot ou la phrase doit être traduit", true);
        optionLangueTraduit.addChoice("Français", "french");
        optionLangueTraduit.addChoice("Anglais", "english");

        commandCreateAction = commandCreateAction.addOptions(optionATraduire, optionLangueUtilisee,
                optionLangueTraduit);
        commandCreateAction.queue();
    }

    /**
     * Créer la commande /convertir
     *
     * La commande convertir permet de convertir une unité de mesure vers une autre unité de mesure
     *
     * @param guild Le guild du DiscordBot
     */
    private void CreerCommandeConvertir(Guild guild) {
        CommandCreateAction commandCreateAction = guild.upsertCommand(Commande.CONVERTIR.transformerEnMinuscule(),
                        "Convertit une unité de mesure vers une autre unité de mesure");
        OptionData optionNombre = new OptionData(OptionType.INTEGER, OptionCommande.NOMBRE.transformerEnMinuscule(),
                "Le nombre de l'unité de mesure", true);
        OptionData optionUniteMesure = new OptionData(OptionType.STRING,
                OptionCommande.UNITEMESURE.transformerEnMinuscule(), "L'unité de mesure du nombre",
                true);
        ajouterChoixUniteMesure(optionUniteMesure);

        OptionData optionUniteMesureVoulue = new OptionData(OptionType.STRING,
                OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule(),
                "Correspond à l'unité de mesure voulue", true);
        ajouterChoixUniteMesure(optionUniteMesureVoulue);

        commandCreateAction = commandCreateAction.addOptions(optionNombre, optionUniteMesure, optionUniteMesureVoulue);
        commandCreateAction.queue();
    }

    /**
     * Permet d'ajouter des choix d'unités de mesure pour les options d'unité de mesure
     *
     * @param optionUniteMesure L'option de l'unité de mesure (uniteMesure ou uniteMesureVoulue)
     */
    private void ajouterChoixUniteMesure(OptionData optionUniteMesure) {
        optionUniteMesure.addChoice("kilomètre", "kilomètre");
        optionUniteMesure.addChoice("mètre", "mètre");
        optionUniteMesure.addChoice("décimètre", "décimètre");
        optionUniteMesure.addChoice("centimètre", "centimètre");
        optionUniteMesure.addChoice("millimètre", "millimètre");
        optionUniteMesure.addChoice("heure", "heure");
        optionUniteMesure.addChoice("minute", "minute");
        optionUniteMesure.addChoice("seconde", "seconde");
        optionUniteMesure.addChoice("année", "année");
        optionUniteMesure.addChoice("kilogramme", "kilogramme");
        optionUniteMesure.addChoice("gramme", "gramme");
    }
}
