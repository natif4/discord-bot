package serpAPI;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import serpapi.GoogleSearch;
import serpapi.SerpApiSearchException;

import java.util.Map;

/**
 * Classe permettant d'obtenir sur l'API SerpAPI une définition pour un mot
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public class DefinisseurAPI extends SerpAPI {

    /**
     * {@inheritDoc}
     */
    @Override
    public String ObtenirResultatRecherche(Map parametreRecherche) {
        GoogleSearch rechercheGoogle = CreerGoogleRecherche(parametreRecherche);

        String resultatRecherche = null;
        try {
            JsonObject donnees = rechercheGoogle.getJson();

            JsonArray resultats = donnees.get("answer_box").getAsJsonObject().get("definitions").getAsJsonArray();
            resultatRecherche = resultats.getAsJsonArray().get(0).getAsString();
        }
        catch (SerpApiSearchException | NullPointerException exception) {
            exception.printStackTrace();
        }

        return resultatRecherche;
    }
}
