package serpAPI;

import serpapi.GoogleSearch;

import java.util.Map;

/**
 * Classe permettant de communiquer avec l'API SerpAPI
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public abstract class SerpAPI {

    /**
     * Crée une instance de GoogleSearch avec le paramètre de la recherche attribué
     *
     * @param parametreRecherche Le paramètre de la recherche permettant de créer l'instance de GoogleSearch
     * @return L'instance de GoogleSearch avec le paramètre de la recherche attribué
     */
    public GoogleSearch CreerGoogleRecherche(Map parametreRecherche) {
       return new GoogleSearch(parametreRecherche);
    }

    /**
     * Permet d'obtenir le resultat de la recherche effectué par l'API SerpAPI
     *
     * @param parametreRecherche Le paramètre de la recherche permettant de créer la recherche Google
     * @return Le résultat de la recherche effectué par l'API SerpAPI
     */
    public abstract String ObtenirResultatRecherche(Map parametreRecherche);
}
